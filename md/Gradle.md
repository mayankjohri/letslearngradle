# Gradle

## Introduction

Gradle is an advanced  build automation tool. It is inspired from other build tools such as ANT, Maven and lvy repositories. It also supports groovy based Domain Specific Language (DSL).   

It can be used to build projects in multiple languages, such as Java, C/C++, Kotlin, Python etc. Gradle can build projects blazingly fast due to various techniques used by it from compiler avoidance to advanced caching and more. 

Gradle has been among the top 20 open-source proejct and is the default build solution in Android Studio. It has seen about 25+ million downloads/month and is supported by almost all IDEs such as Android Studio, IDEA, eclipse, NetBeans

You can get more details at https://gradle.org/guides/. 

### Benefits and Features

#### Features

-  Declarative builds



### Installing Gradle

### Gradle's domain-specific language

### Gradle Wrapper
###  Executing a Gradle Build
### Build files
### Tasks
### DAG & Build lifecycle phases
### Plugins and domain objects
### Documentation
## Building Java Application

### Building a Java project
### Java source code and JAR file
### Java application plugin

### Understanding dependency
### Dependency Tree
### Project dependency

### Gradle - Plugins

### Publishing libraries
## Java Project Testing 

### JUnit
## Executing tests
### Gradle – Multi-Project Build

### Gradle – Deployment

### 

### Conclusion